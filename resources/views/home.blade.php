<?php use Carbon\Carbon; ?>
@extends('layouts.layout')

@section('content')
    <!-- Counts Section -->
      <section class="dashboard-counts section-padding">
        <div class="container-fluid">
          <div class="row">
            <!-- Count item widget-->
            <div class="col-xl-3 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="icon-user"></i></div>
                <div class="name"><strong class="text-uppercase">Total Clients</strong><span>Last 7 days</span>
                <div class="count-number">{{ $data['total_client'] }}</div>
                </div>
              </div>
            </div>
            <!-- Count item widget-->
            <div class="col-xl-3 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="icon-user"></i></div>
                <div class="name"><strong class="text-uppercase">Total Suppliers</strong><span>Last 7 days</span>
                  <div class="count-number">{{ $data['total_supplier'] }}</div>
                </div>
              </div>
            </div>
            <!-- Count item widget-->
            <div class="col-xl-3 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="icon-padnote"></i></div>
                <div class="name"><strong class="text-uppercase">Work Orders</strong><span>Last 5 days</span>
                  <div class="count-number">{{ $data['total_transaction'] }}</div>
                </div>
              </div>
            </div>
            <!-- Count item widget-->
            <div class="col-xl-3 col-md-4 col-6">
              <div class="wrapper count-title d-flex">
                <div class="icon"><i class="icon-bill"></i></div>
                <div class="name"><strong class="text-uppercase">New Products</strong><span>Last 2 days</span>
                  <div class="count-number">{{ $data['total_product'] }}</div>
                </div>
              </div>
            </div>
        </div>
      </section>
    
    {{-- Container --}}
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route("home") }}">Home</a></li>
                <li class="breadcrumb-item active">Transaction today's</li>
                </ul>
            </div>
        </div>
        {{-- End of Breadcrumb --}}
        <hr>

        {{-- Section --}}
        <section>
            <div class="row">
                <div class="col-md-12">

                    {{-- Table --}}
                    <div class="table table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="dataTable">
                            <thead class="text-center">
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Information</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transactions as $index => $item)
                                    <tr>
                                        <td class="text-center">{{$index+1}}</td>
                                        <td class="text-center">{{$item->type}}</td>
                                        <td>{{$item->information}}</td>
                                        <?php $date = Carbon::parse($item->created_at)->format('d M Y') ?>
                                        <td class="text-center">{{$date}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-sm-2">
                                                  <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-info rounded btn-show-transactions"><i class="fa fa-book" data-toggle="modal" data-target=".bd-form-transactions-modal-lg"></i></button>
                                                </div>
                                                @role('super_admin')
                                                <div class="col-sm-2">
                                                    <form class="delete" action="{{action('TransactionController@destroy', $item->id)}}" method="POST">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-lg fa-trash"></i></button>
                                                    </form>
                                                </div>
                                                @endrole
                                            </div>  
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- End of table --}}
                    
                    <!-- Modal -->
                    <div class="modal fade bd-form-transactions-modal-lg" id="modalShowTransaction" tabindex="-1" role="dialog" aria-labelledby="showTransaction" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Detail Transaction</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container">
                                        <table class="table table-transaction">
                                            <tr>
                                                <td>Type</td>
                                                <td> : </td>
                                                <td id="show-transaction-type"></td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td> : </td>
                                                <td id="show-transaction-price"></td>
                                            </tr>
                                            <tr>
                                                <td>Total Product</td>
                                                <td> : </td>
                                                <td id="show-transaction-total-product"></td>
                                            </tr>
                                            <tr>
                                                <td>Total Price</td>
                                                <td> : </td>
                                                <td id="show-transaction-total-price"></td>
                                            </tr>
                                            <tr>
                                                <td>Information</td>
                                                <td> : </td>
                                                <td id="show-transaction-information"></td>
                                            </tr>
                                            <tr>
                                                <td>Created at</td>
                                                <td> : </td>
                                                <td id="show-transaction-date"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        {{-- End of Section --}}
    </div>
    {{-- End of container --}}

    <!-- Statistics Section-->
      <section class="statistics">
        <div class="container-fluid">
          <div class="row d-flex">
            <div class="col-lg-4">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="icon-line-chart"></i></div>
                <div class="number">126,418</div><strong class="text-primary">All Income</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <!-- Monthly Usage-->
              <div class="card data-usage">
                <h2 class="display h4">Monthly Usage</h2>
                <div class="row d-flex align-items-center">
                  <div class="col-sm-6">
                    <div id="progress-circle" class="d-flex align-items-center justify-content-center"></div>
                  </div>
                  <div class="col-sm-6"><strong class="text-primary">80.56 Gb</strong><small>Current Plan</small><span>100 Gb Monthly</span></div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <!-- User Actibity-->
              <div class="card user-activity">
                <h2 class="display h4">User Activity</h2>
                <div class="number">210</div>
                <h3 class="h4 display">Social Users</h3>
                <div class="progress">
                  <div role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar bg-primary"></div>
                </div>
                <div class="page-statistics d-flex justify-content-between">
                  <div class="page-statistics-left"><span>Pages Visits</span><strong>230</strong></div>
                  <div class="page-statistics-right"><span>New Visits</span><strong>73.4%</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

@endsection