<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inventory | Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/font-awesome/css/font-awesome.min.css")}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/fontastic.css")}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/grasp_mobile_progress_circle-1.0.0.min.css")}}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css")}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/style.default.css")}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/custom.css")}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset("public/distribution/img/favicon.ico")}}">
  </head>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                        <h3 class="text-white">{{ config('app.name', 'Laravel') }} Panel Login</h3>
                </div>
            </div>
        </nav>

        @yield('content')

    </div>

    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>{{config("app.name")}} &copy; {{date('Y')}}</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Design by <a href="#" class="external">Riza Alif Wildani</a></p>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
                </div>
            </div>
        </div>
    </footer>
    {{-- End of footer --}}

    <!-- JavaScript files-->
    <script src="{{asset("public/distribution/vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/popper.js/umd/popper.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("public/distribution/js/grasp_mobile_progress_circle-1.0.0.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/jquery.cookie/jquery.cookie.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/chart.js/Chart.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/jquery-validation/jquery.validate.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js")}}"></script>
    <script src="{{asset("public/distribution/js/charts-home.js")}}"></script>
    <!-- Main File-->
    <script src="{{asset("public/distribution/js/front.js")}}"></script>
</body>
{{-- End of body --}}

</html>
