<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inventory</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/font-awesome/css/font-awesome.min.css")}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/fontastic.css")}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/grasp_mobile_progress_circle-1.0.0.min.css")}}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{asset("public/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css")}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/style.default.css")}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset("public/distribution/css/custom.css")}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset("public/distribution/img/favicon.ico")}}">
    <!-- Favicon-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap4.min.css">
  </head>

  <body>

    <!-- Side Navbar -->
    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <!-- User Info-->
          
          <div class="sidenav-header-inner text-center">
            @if (auth()->user()->image)
              <img src="{{ url(auth()->user()->image)}}" alt="person" class="img-fluid rounded-circle">  
            @endif
            <h2 class="h5">{{auth()->user()->name}}</h2>
          </div>
          <!-- Small Brand information, appears on minimized sidebar-->
          <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>I</strong><strong class="text-primary">T</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          {{-- <h5 class="sidenav-heading">Main</h5> --}}
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="{{route('home')}}"> <i class="icon-home"></i>Home </a></li>
            
            @role ("super_admin")
            
              <li><a href="{{ route('users.index') }}"> <i class="icon-user"></i>Users</a></li>
              
              @endrole
              
              <li><a href="#dataDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Data </a>
                <ul id="dataDropdown" class="collapse list-unstyled ">
                  @role ("super_admin")
                  <li><a href="{{route('roles.index')}}"> <i class="fa fa-table"></i>Roles </a></li>
                  @endrole
                  <li><a href="{{route('products.index')}}"> <i class="fa fa-table"></i>Products </a></li>
                  <li><a href="{{route('suppliers.index')}}"> <i class="fa fa-users"></i>Suppliers </a></li>
                <li><a href="{{route('clients.index')}}"> <i class="fa fa-users"></i>Clients </a></li>
              </ul>
            </li>
            <li><a href="#transactionDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-cart-arrow-down"></i>Transaction </a>
              <ul id="transactionDropdown" class="collapse list-unstyled ">
                <li><a href="{{route('incomings.index')}}"> <i class="fa fa-table"></i>Incoming product</a></li>
                <li><a href="{{route('spendings.index')}}"> <i class="fa fa-table"></i>Spending product </a></li>
                @role ("super_admin")
                <li><a href="{{route('transactions.index')}}"> <i class="fa fa-table"></i>Data Transaction </a></li>
                @endrole
              </ul>
            </li>
          </ul>
        </div>
        {{-- End of sidebar menu --}}
      </div>
    </nav>
    {{-- End of sidebar --}}

    <div class="page">

      <!-- navbar-->
      <header class="header">

        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
            <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="{{route('home')}}" class="navbar-brand">
                  <div class="brand-text d-none d-md-inline-block"><strong class="text-primary">{{config("app.name")}}</strong></div></a></div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Notifications dropdown-->
                {{-- <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                          <div class="notification-time"><small>10 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>
                  </ul>
                </li> --}}
                <!-- Messages dropdown-->
                {{-- <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope"></i><span class="badge badge-info">10</span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="{{asset("public/distribution/img/avatar-1.jpg")}}" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="{{asset("public/distribution/img/avatar-2.jpg")}}" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="{{asset("public/distribution/img/avatar-3.jpg")}}" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-envelope"></i>Read all messages    </strong></a></li>
                  </ul>
                </li> --}}
                <!-- Languages dropdown    -->
                {{-- <li class="nav-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle"><img src="{{asset("public/distribution/img/flags/16/GB.png")}}" alt="English"><span class="d-none d-sm-inline-block">English</span></a>
                  <ul aria-labelledby="languages" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="{{asset("public/distribution/img/flags/16/DE.png")}}" alt="English" class="mr-2"><span>German</span></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="{{asset("public/distribution/img/flags/16/FR.png")}}" alt="English" class="mr-2"><span>French</span></a></li>
                  </ul>
                </li> --}}
                <!-- Log out-->
                <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
        {{-- End of navbar --}}

      </header>
      {{-- End of header --}}


      <!-- Counts Section -->
      <section class="dashboard-counts section-padding">
        
        @yield('content')

      </section>


      <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>{{config("app.name")}} &copy; 2019</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Design by <a href="#" class="external">Riza Alif Wildani</a></p>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
                </div>
            </div>
        </div>
    </footer>
      {{-- End of footer --}}

    </div>
    {{-- End of page --}}

    <!-- JavaScript files-->
    <script src="{{asset("public/distribution/vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/popper.js/umd/popper.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("public/distribution/js/grasp_mobile_progress_circle-1.0.0.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/jquery.cookie/jquery.cookie.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/chart.js/Chart.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/jquery-validation/jquery.validate.min.js")}}"></script>
    <script src="{{asset("public/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js")}}"></script>
    <script src="{{asset("public/distribution/js/charts-home.js")}}"></script>
    <!-- Main File-->
    <script src="{{asset("public/distribution/js/front.js")}}"></script>
    <script src="{{asset("public/js/script.js")}}"></script>
    {{-- Data Table --}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#dataTable').DataTable({
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ],
          fixedHeader: true
        });
      }) 
    </script>
  </body>
  {{-- End of body --}}

</html>