@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Products</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <div class="row">
                    <div class="col-md-12">
    
                        {{-- Table --}}
                        <div class="table table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Supplier</th>
                                        <th><a href="{{ route('incomings.index') }}" class="btn btn-sm btn-outline-success rounded">Create</a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($products as $index => $item)

                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td class="text-right">Rp. {{ $item->price }}</td>
                                            <td class="text-center">{{ $item->stock }}</td>
                                            <td>{{ $item->supplier->name }}</td>
                                            <td>
                                                <div class="row justify-content-center">
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-info rounded btn-show-product"><i class="fa fa-book"></i></button>
                                                    </div>
                                                    
                                                    @role('super_admin')
                                                    <div class="col-2">
                                                        <button type="button" id="btn-edit-product" data-id="{{ $item->id }}" class="btn btn-sm btn-warning rounded btn-edit-product"><i class="fa fa-edit"></i></button>
                                                    </div>

                                                    <div class="col-2">
                                                        <form class="delete" action="{{action('ProductController@destroy', $item->id)}}" method="POST">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                    @endrole
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{-- End of table --}}
    
                    </div>
                </div>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

        {{-- Modal --}}
        <div id="productShowModal" class="modal fade bd-form-product-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table">
                            <tr>
                                <td>Name</td>
                                <td> : </td>
                                <td id="show-name"></td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td> : </td>
                                <td id="show-price"></td>
                            </tr>
                            <tr>
                                <td>Stock</td>
                                <td> : </td>
                                <td id="show-stock"></td>
                            </tr>
                            <tr>
                                <td>Supplier</td>
                                <td> : </td>
                                <td id="show-supplier"></td>
                            </tr>
                            <tr>
                                <td>Created at</td>
                                <td> : </td>
                                <td id="show-date"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal --}}
        <div id="productEditShowModal" class="modal fade bd-form-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table table-responsive">
    
                            <form id="form-update-product" action="" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" id="method" name="_method" value="PUT">

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="update-name" name="name" required autofocus class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input id="update-price" name="price" required class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="stock">Stock</label>
                                    <input id="update-stock" name="stock" required class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="supplier">Supplier</label>
                                    <select class="form-control" name="supplier" id="update-supplier">
                                        <option value="">Select Supplier</option>

                                        @foreach ($suppliers as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Update</button>
                                    </div>
                                </div>

                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection