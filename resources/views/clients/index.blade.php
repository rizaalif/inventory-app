@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Client</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <div class="row">
                    <div class="col-md-12">
    
                        {{-- Table --}}
                        <div class="table">
                            <table class="table table-bordered table-striped table-hover" id="dataTable">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th><button type="button" id="btn-create-client" class="btn btn-sm btn-outline-success rounded">Create</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($clients as $index => $item)

                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td class="text-center">{{ $item->phone }}</td>
                                            <td>
                                                <div class="row justify-content-center">
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-info rounded btn-show-client"><i class="fa fa-book" data-toggle="modal" data-target=".bd-form-client-modal-lg"></i></button>
                                                    </div>
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-warning rounded btn-edit-client"><i class="fa fa-edit"></i></button>
                                                    </div>
                                                    
                                                    @role('super_admin')
                                                    <div class="col-2">
                                                        <form class="delete" action="{{action('ClientController@destroy', $item->id)}}" method="POST">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                    @endrole
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{-- End of table --}}
    
                    </div>
                </div>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

        {{-- Modal --}}
        <div id="clientShowModal" class="modal fade bd-form-client-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table">
                            <tr>
                                <td>Name</td>
                                <td> : </td>
                                <td id="show-name"></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td> : </td>
                                <td id="show-address"></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td> : </td>
                                <td id="show-phone"></td>
                            </tr>
                            <tr>
                                <td>Created at</td>
                                <td> : </td>
                                <td id="show-date"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal Form --}}
        <div id="clientShowFormModal" class="modal fade bd-form-client-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title modal-form-title">Create Client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table table-responsive">

                            <form id="form-client" action="{{ action('ClientController@store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" id="method" name="_method" value="POST">

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" name="name" required autofocus class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input id="address" name="address" required class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input id="phone" name="phone" required class="form-control" type="text">
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Save</button>
                                    </div>
                                </div>

                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection