@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Transaction | Spending</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <form id="form-spending" action="{{ action('SpendingController@store') }}" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="client">Client</label>
                                <select class="form-control" name="client" id="client">
                                    <option value="">Select Client</option>

                                    @foreach ($clients as $own)
                                        <option value="{{ $own->id }}">{{ $own->name }}</option>
                                    @endforeach
                                </select>
                            </div>
        
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="product">Product</label>
                                <select class="form-control" name="product" id="product">
                                    <option value="">Select Product</option>

                                    @foreach ($products as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="total_products">Total Products</label>
                                <input id="total_products" name="total_products" required class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="information">Information</label>
                                <textarea class="form-control" name="information" id="information" cols="10" rows="5" placeholder="<-- Optional -->"></textarea>
                            </div>
                        </div>

                        <hr>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

@endsection