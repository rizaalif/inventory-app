@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Users Management</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <div class="row">
                    <div class="col-md-12">
    
                        {{-- Table --}}
                        <div class="table table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>
                                            <button type="button" id="btn-create-user" class="btn btn-sm btn-outline-success rounded">Create</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($users as $index => $user)

                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td class="text-center">
                                                @foreach ($user->roles as $index => $role)
                                                    {{ $role->display_name }}
                                                    {{ $index+1 < $user->roles->count() ? ',' : ''}}
                                                @endforeach
                                            </td>
                                            <td>
                                                <div class="row justify-content-center">
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $user->id }}" class="btn btn-sm btn-info rounded btn-upload"><i class="fa fa-upload"></i></button>
                                                    </div>
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $user->id }}" class="btn btn-sm btn-info rounded btn-show-user"><i class="fa fa-book" data-toggle="modal" data-target=".bd-form-user-modal-lg"></i></button>
                                                    </div>
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $user->id }}" class="btn btn-sm btn-warning rounded btn-edit-user"><i class="fa fa-edit"></i></button>
                                                    </div>
                                                    
                                                    @role('super_admin')
                                                    <div class="col-2">
                                                        <form class="delete" action="{{action('UserController@destroy', $user->id)}}" method="POST">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                    @endrole
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{-- End of table --}}
    
                    </div>
                </div>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

        {{-- Modal --}}
        <div id="userShowModal" class="modal fade bd-form-user-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <img id="show-img" width="30%" height="auto" src="" alt="person" class="img-fluid rounded-circle"> 
                        <table class="table">
                            <tr>
                                <td>Name</td>
                                <td> : </td>
                                <td id="show-name"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td> : </td>
                                <td id="show-email"></td>
                            </tr>
                            <tr>
                                <td>Created at</td>
                                <td> : </td>
                                <td id="show-date"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal Form --}}
        <div id="userShowFormModal" class="modal fade bd-form-user-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title modal-form-title">Create user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table table-responsive">

                            <form id="form-user" enctype="multipart/form-data" action="{{ action('UserController@store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" id="method" name="_method" value="POST">

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" name="name" required autofocus class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" name="email" required class="form-control" type="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" name="password" required class="form-control" type="password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirmation Password</label>
                                    <input id="password_confirmation" name="password_confirmation" required class="form-control" type="password">
                                </div>
                                <div class="form-group">
                                    <label for="role">Roles</label>
                                    <div class="checkbox"">
                                        <label class="btn btn-outline-primary">
                                            <input type="checkbox" id="super_admin" value="super_admin" name="roles[]"> Super Admin
                                        </label>
                                    </div>

                                    <div class="checkbox"">
                                        <label class="btn btn-outline-primary">
                                            <input type="checkbox" id="user" value="user" name="roles[]"> User
                                        </label>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="profil_image"><h3>Upload User Image</h3></label>
                                    <input required name="profile_image" class="form-control-file" type="file">
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Save</button>
                                    </div>
                                </div>

                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal Form --}}
        <div id="userShowUpdateModal" class="modal fade bd-form-user-update-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title modal-form-title">Update User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="container">
                            <table class="table table-responsive">
    
                                <form id="form-update-user" action="" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="method" name="_method" value="PUT">
    
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input id="update-name" name="name" required autofocus class="form-control" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="update-email" disabled name="email" required class="form-control" type="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Roles</label>
                                        <div class="checkbox"">
                                            <label class="btn btn-outline-primary">
                                                <input type="checkbox" id="update_super_admin" value="super_admin" name="roles[]"> Super Admin
                                            </label>
                                        </div>
    
                                        <div class="checkbox"">
                                            <label class="btn btn-outline-primary">
                                                <input type="checkbox" id="update_user" value="user" name="roles[]"> User
                                            </label>
                                        </div>
                                    </div>
    
                                    <hr>
    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Update</button>
                                        </div>
                                    </div>
    
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Modal Upload Image --}}
            <div id="modalUpload" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ action('ImageController@store') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="profil_image"><h3>Upload User Image</h3></label>
                                    <hr>
                                    <input type="hidden" name="user_id" id="user_id">
                                    <input id="profil_image" required name="profile_image" class="form-control-file" type="file">
                                </div>
                                <hr>
                                <button class="btn btn-primary rounded" type="submit">Upload</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

@endsection