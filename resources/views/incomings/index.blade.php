@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Transaction | Incoming</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <form id="form-incoming" action="{{ action('IncomingController@store') }}" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="supplier">Supplier</label>
                                <select class="form-control" name="supplier" id="supplier">
                                    <option value="">Select Supplier</option>

                                    @foreach ($suppliers as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
        
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="product">Product</label>
                                <input id="product" name="product" required class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input id="price" name="price" required class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="total_products">Total Products</label>
                                <input id="total_products" name="total_products" required class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="information">Information</label>
                                <textarea class="form-control" name="information" id="information" cols="10" rows="5" placeholder="<-- Optional -->"></textarea>
                            </div>
                        </div>

                        <hr>

                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

@endsection