@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Roles</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <div class="row">
                    <div class="col-md-12">
    
                        {{-- Table --}}
                        <div class="table table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Role name</th>
                                        <th>Display Name</th>
                                        <th>Description</th>
                                        <th><button type="button" id="btn-create-role" class="btn btn-sm btn-outline-success rounded">Create</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($roles as $index => $item)

                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->display_name }}</td>
                                            <td class="text-center">{{ $item->description }}</td>
                                            <td>
                                                <div class="row justify-content-center">
                                                    <div class="col-2 mr-2">
                                                        <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-warning rounded btn-edit-role"><i class="fa fa-edit"></i></button>
                                                    </div>
                                                    
                                                    <div class="col-2">
                                                        <form class="delete" action="{{action('RoleController@destroy', $item->id)}}" method="POST">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{-- End of table --}}
    
                    </div>
                </div>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

        {{-- Modal Form --}}
        <div id="roleShowFormModal" class="modal fade bd-form-role-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title modal-form-title">Create Role</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <table class="table table-responsive">

                            <form id="form-role" action="{{ action('RoleController@store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" id="method" name="_method" value="POST">

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" name="name" required autofocus class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="display_name">Display Name</label>
                                    <input id="display_name" name="display_name" required class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input id="description" name="description" required class="form-control" type="text">
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btn-action" class="btn btn-outline-primary form-control">Save</button>
                                    </div>
                                </div>

                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection