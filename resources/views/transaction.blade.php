<?php use Carbon\Carbon; ?>
@extends('layouts.layout')

@section('content')
    
    {{-- Container --}}
    <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item active">Transaction</li>
                    </ul>
                </div>
            </div>
            {{-- End of Breadcrumb --}}
            <hr>

            @if (session()->get('key') != "")
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{session()->get('key')}}</strong> 
                </div>
                
                <script>
                    $(".alert").alert();
                </script>
            @endif
    
            {{-- Section --}}
            <section>
                <div class="row">
                    <div class="col-md-12">
    
                        {{-- Table --}}
                    <div class="table table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="dataTable">
                            <thead class="text-center">
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Information</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transactions as $index => $item)
                                    <tr>
                                        <td class="text-center">{{$index+1}}</td>
                                        <td class="text-center">{{$item->type}}</td>
                                        <td>{{$item->information}}</td>
                                        <?php $date = Carbon::parse($item->created_at)->format('d M Y') ?>
                                        <td class="text-center">{{$date}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-sm-2">
                                                    <div class="col-2">
                                                        <button type="button" data-id="{{ $item->id }}" class="btn btn-sm btn-info rounded btn-show-transactions"><i class="fa fa-book" data-toggle="modal" data-target=".bd-form-transactions-modal-lg"></i></button>
                                                    </div>
                                                </div>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade bd-form-transactions-modal-lg" id="modalShowTransaction" tabindex="-1" role="dialog" aria-labelledby="showTransaction" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Detail Transaction</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="container">
                                                                    <table class="table table-transaction">
                                                                        <tr>
                                                                            <td>Type</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-type"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Price</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-price"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Product</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-total-product"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Price</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-total-price"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Information</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-information"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Created at</td>
                                                                            <td> : </td>
                                                                            <td id="show-transaction-date"></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @role('super_admin')
                                                <div class="col-sm-2">
                                                    <form class="delete" action="{{action('TransactionController@destroy', $item->id)}}" method="POST">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn btn-sm btn-danger rounded"><i class="fa fa-lg fa-trash"></i></button>
                                                    </form>
                                                </div>
                                                @endrole
                                            </div>  
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- End of table --}}
    
                    </div>
                </div>
            </section>
            {{-- End of Section --}}
        </div>
        {{-- End of container --}}

@endsection