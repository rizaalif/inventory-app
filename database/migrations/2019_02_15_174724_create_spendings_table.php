<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spendings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id')->on('id')->onDelete('cascade');
            $table->unsignedInteger('client_id')->on('id')->onDelete('cascade');
            $table->unsignedInteger('product_id')->on('id')->onDelete('cascade');
            $table->integer('total_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spendings');
    }
}
