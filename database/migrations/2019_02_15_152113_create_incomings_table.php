<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incomings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id')->on('id')->onDelete('cascade');
            $table->unsignedInteger('supplier_id')->on('id')->onDelete('cascade');
            $table->unsignedInteger('product_id')->on('id')->onDelete('cascade');
            $table->integer('total_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incomings');
    }
}
