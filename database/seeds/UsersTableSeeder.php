<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'id' => Uuid::uuid4(),
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('secret'),
            'image' => 'http://localhost/inventory-app/public/distribution/img/avatar-7.jpg'
        ]);

        $user->attachRole('super_admin');

    }
}
