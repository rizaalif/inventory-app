<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            App\Model\Product::create([
                'name' => $faker->name,
                'price' => $faker->numberBetween(1000,9000),
                'stock' => $faker->numberBetween(0,50),
                'supplier_id' => 1
            ]);
        }
    }
}
