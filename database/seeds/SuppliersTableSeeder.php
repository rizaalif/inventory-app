<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            App\Model\Supplier::create([
                'name' => $faker->name,
                'address' => $faker->word,
                'phone' => $faker->phoneNumber,
            ]);
        }
    }
}
