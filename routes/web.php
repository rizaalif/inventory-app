<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route Auth
Auth::routes();

// Route Logout
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {

    Route::resource('users', 'UserController');

    // Route Home
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/transactions/{id}', 'HomeController@show')->name('transaction.show');
    Route::get('/transactions', 'TransactionController@index')->name('transactions.index');
    Route::delete('/transactions/{id}', 'TransactionController@destroy')->name('transactions.destroy');

    // Route Product
    Route::resource('products', 'ProductController');

    // Route Supplier
    Route::resource('suppliers', 'SupplierController');

    // Route Role
    Route::resource('roles', 'RoleController');

    // Route Client
    Route::resource('clients', 'ClientController');

    // Route Incoming
    Route::resource('incomings', 'IncomingController');

    // Route Spending
    Route::resource('spendings', 'SpendingController');

    // Route Image
    Route::resource('images', 'ImageController');

});
