$(document).ready(function () {
  // Product Show
  $(".btn-show-product").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "products/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#productShowModal").modal("show");
        $("#show-name").text(data.name);
        $("#show-price").text("Rp. " + data.price);
        $("#show-stock").text(data.stock);
        $("#show-supplier").text(data.supplier_name);
        $("#show-date").text(data.date);
      }
    });
  });

  // Transaction Show
  $(".btn-show-transactions").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "transactions/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        console.log(data);
        if (data.type === "IN") {
          $(".table-transaction")
            .find($(".client-name"))
            .remove();
          $(".table-transaction").append(
            `
            <tr class="supplier-name">
                <td>Supplier Name</td>
                <td> : </td>
                <td id="show-supplier">` +
            data.supplier_name +
            `</td>
            </tr>
          `
          );
        } else {
          $(".table-transaction")
            .find($(".supplier-name"))
            .remove();
          $(".table-transaction").append(
            `
            <tr class="client-name">
                <td>Client Name</td>
                <td> : </td>
                <td id="show-client">` +
            data.client_name +
            `</td>
            </tr>
          `
          );
        }
        $("#modalShowTransaction").modal("show");
        $("#show-transaction-type").text(data.type);
        $("#show-transaction-price").text("Rp. " + data.price);
        $("#show-transaction-total-product").text(data.total_product);
        $("#show-transaction-total-price").text("Rp. " + data.total_price);
        $("#show-transaction-information").text(data.information);
        $("#show-transaction-date").text(data.date);
      }
    });
  });

  // Supplier Show
  $(".btn-show-supplier").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "suppliers/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#supplierShowModal").modal("show");
        $("#show-name").text(data.name);
        $("#show-address").text(data.address);
        $("#show-phone").text(data.phone);
        $("#show-date").text(data.date);
      }
    });
  });

  // Supplier Create
  $("#btn-create-supplier").click(function (e) {
    $("#supplierShowFormModal").modal("show");
    $(".modal-form-title").text("Create Supplier");
    $("#form-supplier").attr("action", "suppliers");
    $("#btn-action").text("Save");
    $("#method").val("POST");
    $("#name").val("");
    $("#address").val("");
    $("#phone").val("");
  });

  // Supplier Update
  $(".btn-edit-supplier").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "suppliers/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#supplierShowFormModal").modal("show");
        $(".modal-form-title").text("Update Supplier");
        $("#form-supplier").attr("action", "suppliers/" + data.id + "");
        $("#btn-action").text("Update");
        $("#method").val("PATCH");
        $("#name").val(data.name);
        $("#address").val(data.address);
        $("#phone").val(data.phone);
      }
    });
  });

  // Role Create
  $("#btn-create-role").click(function (e) {
    $("#roleShowFormModal").modal("show");
    $(".modal-form-title").text("Create Role");
    $("#form-role").attr("action", "roles");
    $("#btn-action").text("Save");
    $("#method").val("POST");
    $("#name").val("");
    $("#display_name").val("");
    $("#description").val("");
  });

  // Role Update
  $(".btn-edit-role").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "roles/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#roleShowFormModal").modal("show");
        $(".modal-form-title").text("Update Role");
        $("#form-role").attr("action", "roles/" + data.id + "");
        $("#btn-action").text("Update");
        $("#method").val("PATCH");
        $("#name").val(data.name);
        $("#display_name").val(data.display_name);
        $("#description").val(data.description);
      }
    });
  });

  // Client Show
  $(".btn-show-client").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "clients/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#clientShowModal").modal("show");
        $("#show-name").text(data.name);
        $("#show-address").text(data.address);
        $("#show-phone").text(data.phone);
        $("#show-date").text(data.date);
      }
    });
  });

  // Client Create
  $("#btn-create-client").click(function (e) {
    $("#clientShowFormModal").modal("show");
    $(".modal-form-title").text("Create Client");
    $("#form-client").attr("action", "clients");
    $("#btn-action").text("Save");
    $("#method").val("POST");
    $("#name").val("");
    $("#address").val("");
    $("#phone").val("");
  });

  // Client Update
  $(".btn-edit-client").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "clients/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#clientShowFormModal").modal("show");
        $(".modal-form-title").text("Update Client");
        $("#form-client").attr("action", "clients/" + data.id + "");
        $("#btn-action").text("Update");
        $("#method").val("PATCH");
        $("#name").val(data.name);
        $("#address").val(data.address);
        $("#phone").val(data.phone);
      }
    });
  });

  // User Show
  $(".btn-show-user").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "users/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#userShowModal").modal("show");
        $("#show-name").text(data.name);
        $("#show-img").attr('src', data.image);
        $("#show-email").text(data.email);
        $("#show-date").text(data.date);
      }
    });
  });

  // User Create
  $("#btn-create-user").click(function (e) {
    $("#userShowFormModal").modal("show");
    $("#password_confirmation").attr("disabled", "disabled");
  });

  $("#password").keyup(function () {
    if ($("#password").val() === "") {
      $("#password_confirmation").attr("disabled", "disabled");
    } else {
      $("#password_confirmation").removeAttr("disabled");
    }
  });

  if ($("#password").val() === $("#password_confirmation").val()) {
    $("#btn-create-user").removeAttr("disabled");
  } else {
    $("#password_confirmation").attr("disabled", "disabled");
  }

  // User Update
  $(".btn-edit-user").click(function (e) {
    let id = $(this).attr("data-id");

    $.ajax({
      url: "users/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#userShowUpdateModal").modal("show");
        $("#form-update-user").attr("action", "users/" + data.id + "");
        $("#update-name").val(data.name);
        $("#update-email").val(data.email);
        $.each(data.roles, function (index, obj) {
          if (obj.name === "super_admin") {
            $("#update_super_admin").attr("checked", true);
          } else {
            $("#update_user").attr("checked", true);
          }
        });
      }
    });
  });

  // Upload
  $(".btn-upload").click(function (e) {
    e.preventDefault();

    $("#modalUpload").modal("show");
    $("#user_id").val($(this).attr("data-id"));
  });

  // Product Update
  $(".btn-edit-product").click(function (e) {
    let id = $(this).attr("data-id");
    $.ajax({
      url: "products/" + id,
      type: "GET",
      data: {},
      success: function (data) {
        data = JSON.parse(data);
        $("#productEditShowModal").modal("show");
        $("#form-update-product").attr("action", "products/" + data.id);
        $("#update-name").val(data.name);
        $("#update-price").val(data.price);
        $("#update-stock").val(data.stock);
        $("#update-supplier").val(data.supplier_id);
      }
    });
  });

  // Delete
  $(".delete").submit(function () {
    return confirm("Do you want to delete this data?");
  });
});
