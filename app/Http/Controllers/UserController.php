<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Model\Image;
use Image as ImageInt;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Auth;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:super_admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'DESC')->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user_id = Uuid::uuid4();
        $user = new User();
        $user->id = $user_id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if ($user) {
            $user->syncRoles($request->roles);

            if ($request->hasFile('profile_image')) {

            //get filename with extension
                $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
     
            //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
     
            //get file extension
                $extension = $request->file('profile_image')->getClientOriginalExtension();
     
            //filename to store
                $filenametostore = $filename . '_' . time() . '.' . $extension;
     
            //Upload File
                $request->file('profile_image')->storeAs('public/profile_images', $filenametostore);
                $request->file('profile_image')->storeAs('public/profile_images/thumbnail', $filenametostore);
     
            // Thumbnail path
                $thumbnailpath = public_path('storage/profile_images/thumbnail/' . $filenametostore);
            // $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save($thumbnailpath);

            // Intervention create resize
                $img = ImageInt::make($thumbnailpath)->resize(354, 476)->save($thumbnailpath);
            
            // Path for save to database
                $path = "public/storage/profile_images/" . $filenametostore;

                if ($img) {

                // Search image state profile from images
                    $imageprofile = Image::where(['state' => 'profile', 'user_id' => $user_id])->first();

                // Set image to null
                    if ($imageprofile) {
                        $imageprofile->update(['state' => null]);
                    }
                
                // Save to table images
                    $create = Image::create([
                        'user_id' => $user_id,
                        'name' => $filenametostore,
                        'path' => url($path),
                        'size' => $img->filesize(),
                        'state' => 'profile',
                    ]);

                //  Update User image
                    if ($create) {
                        User::find($user_id)->update(['image' => url($path)]);
                    } else {
                        $request->session()->flash('key', 'Upload error');
                        return redirect()->back();
                    }

                }
            }

            $request->session()->flash('key', 'successfully created user');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $date = Carbon::parse($user->created_at);
        $newDate = $date->format("d M y");
        $user['date'] = $newDate;
        $roles = $user->roles;
        $user['roles'] = $roles;

        return json_encode($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $update = $user->update([
            'name' => $request->name
        ]);

        if ($update) {
            $user->syncRoles($request->roles);

            $request->session()->flash('key', 'successfully updated user');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete = User::find($id)->delete();

        if ($delete) {
            $request->session()->flash('key', 'successfully deleted user');
            return redirect()->back();
        }
    }
}
