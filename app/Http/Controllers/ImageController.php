<?php

namespace App\Http\Controllers;

use App\User;
use App\Model\Image;
use Image as ImageInt;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('profile_image')) {

            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
     
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
     
            //get file extension
            $extension = $request->file('profile_image')->getClientOriginalExtension();
     
            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;
     
            //Upload File
            $request->file('profile_image')->storeAs('public/profile_images', $filenametostore);
            $request->file('profile_image')->storeAs('public/profile_images/thumbnail', $filenametostore);
     
            // Thumbnail path
            $thumbnailpath = public_path('storage/profile_images/thumbnail/' . $filenametostore);
            // $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save($thumbnailpath);

            // Intervention create resize
            $img = ImageInt::make($thumbnailpath)->resize(354, 476)->save($thumbnailpath);
            
            // Path for save to database
            $path = "public/storage/profile_images/" . $filenametostore;

            if ($img) {

                // Search image state profile from images
                $imageprofile = Image::where(['state' => 'profile', 'user_id' => $request->user_id])->first();

                // Set image to null
                if ($imageprofile) {
                    $imageprofile->update(['state' => null]);
                }
                
                // Save to table images
                $create = Image::create([
                    'user_id' => $request->user_id,
                    'name' => $filenametostore,
                    'path' => url($path),
                    'size' => $img->filesize(),
                    'state' => 'profile',
                ]);

                //  Update User image
                if ($create) {
                    User::find($request->user_id)->update(['image' => url($path)]);

                    $request->session()->flash('key', 'Upload success');
                    return redirect()->back();
                } else {
                    $request->session()->flash('key', 'Upload error');
                    return redirect()->back();
                }

            }
        }
    }
}
