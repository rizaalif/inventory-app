<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\Product;
use App\Model\Supplier;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::all();
        $products = Product::orderBy('created_at', 'DESC')->get();

        return view('products.index', compact('products', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::find($id);
        $date = Carbon::parse($products->created_at);
        $newDate = $date->format("d M y");
        $products['date'] = $newDate;
        $products['supplier_name'] = $products->supplier->name;

        return json_encode($products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'supplier_id' => $request->supplier,
        ]);

        $request->session()->flash('key', 'successfully updated user');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete = Product::find($id)->delete();

        if ($delete) {
            $request->session()->flash('key', 'successfully deleted product');
            return redirect()->back();
        }
    }
}
