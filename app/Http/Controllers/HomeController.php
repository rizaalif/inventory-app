<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\Client;
use App\Model\Product;
use App\Model\Incoming;
use App\Model\Spending;
use App\Model\Supplier;
use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['total_client'] = Client::get()->count();
        $data['total_supplier'] = Supplier::get()->count();
        $data['total_product'] = Product::get()->count();
        $data['total_transaction'] = Transaction::get()->count();
        $date = Carbon::now()->toDateString();
        $transactions = Transaction::where('date', $date)->get();
        return view('home', compact('transactions', 'data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find = Transaction::where('id', $id)->first();
        if ($find->type == "IN") {
            $transactions = Incoming::where('transaction_id', $find->id)->first();
            $transactions['type'] = $find->type;
            $transactions['date'] = $find->date;
            $transactions['information'] = $find->information;
            $transactions["product_name"] = $transactions->product->name;
            $transactions["price"] = $transactions->product->price;
            $transactions["total_product"] = $transactions->total_products;
            $transactions["supplier_name"] = $transactions->supplier->name;
            $price = $transactions->product->price;
            $total_product = $transactions->total_products;
            $transactions["total_price"] = $price * $total_product;
        } else {
            $transactions = Spending::where('transaction_id', $find->id)->first();
            $transactions['type'] = $find->type;
            $transactions['date'] = $find->date;
            $transactions['information'] = $find->information;
            $transactions["product_name"] = $transactions->product->name;
            $transactions["price"] = $transactions->product->price;
            $transactions["total_product"] = $transactions->total_products;
            $transactions["client_name"] = $transactions->client->name;
            $price = $transactions->product->price;
            $total_product = $transactions->total_products;
            $transactions["total_price"] = $price * $total_product;
        }
        return json_encode($transactions);
    }
}
