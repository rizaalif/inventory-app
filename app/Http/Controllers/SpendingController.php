<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\Client;
use App\Model\Product;
use App\Model\Spending;
use App\Model\Transaction;
use Illuminate\Http\Request;

class SpendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        $products = Product::all();

        return view('spendings.index', compact('clients', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::find($request->product);

        $stock = $product->stock;

        if ($stock < $request->total_products) {
            $request->session()->flash('key', 'Stock exceeds demand, Remaining stock : ' . $stock . '');
            return redirect()->back();
        }

        $newStock = $stock - $request->total_products;

        $transaction = Transaction::create([
            'type' => 'OUT',
            'date' => Carbon::now()->format('Y-m-d'),
            'information' => $request->information
        ]);

        if ($transaction) {

            $productUpdate = $product->update([
                'stock' => $newStock,
            ]);

            if ($productUpdate) {

                $spending = Spending::create([
                    'transaction_id' => $transaction->id,
                    'client_id' => $request->client,
                    'product_id' => $product->id,
                    'total_products' => $request->total_products
                ]);

                $request->session()->flash('key', 'successfully created transaction');
                return redirect()->back();

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
