<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\Incoming;
use App\Model\Supplier;
use App\Model\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;

class IncomingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::all();
        $products = Product::all();

        return view('incomings.index', compact('suppliers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = Transaction::create([
            'type' => 'IN',
            'date' => Carbon::now()->format('Y-m-d'),
            'information' => $request->information
        ]);

        if ($transaction) {

            $productscount = Product::where('name', $request->product)->count();

            if ($productscount == 0) {

                $productCreate = Product::create([
                    'name' => $request->product,
                    'price' => $request->price,
                    'stock' => $request->total_products,
                    'supplier_id' => $request->supplier
                ]);

                if ($productCreate) {
                    $product = Product::where('name', $request->product)->first();

                    $incoming = Incoming::create([
                        'transaction_id' => $transaction->id,
                        'supplier_id' => $request->supplier,
                        'product_id' => $product->id,
                        'total_products' => $request->total_products
                    ]);

                    $request->session()->flash('key', 'successfully created transaction');
                    return redirect()->back();

                }

            } else {
                $products = Product::where('name', $request->product)->first();

                $stock = $products->stock;
                $newStock = $stock + $request->total_products;
                $products->Update([
                    'name' => $request->product,
                    'price' => $request->price,
                    'stock' => $newStock,
                    'supplier_id' => $request->supplier
                ]);

                $incoming = Incoming::create([
                    'transaction_id' => $transaction->id,
                    'supplier_id' => $request->supplier,
                    'product_id' => $products->id,
                    'total_products' => $request->total_products
                ]);

                $request->session()->flash('key', 'successfully created transaction');
                return redirect()->back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
