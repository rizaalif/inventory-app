<?php

namespace App\Http\Controllers;

use App\Model\Incoming;
use App\Model\Spending;
use App\Model\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:super_admin']);
    }

    public function index()
    {
        $transactions = Transaction::orderBy('created_at', 'DESC')->get();

        return view('transaction', compact('transactions'));
    }

    public function destroy(Request $request, $id)
    {
        $delete = Transaction::find($id)->delete();

        if ($delete) {
            $incoming = Incoming::where('transaction_id', $id)->count();
            $spending = Spending::where('transaction_id', $id)->count();

            if ($incoming > 0) {
                Incoming::where('transaction_id', $id)->delete();
            }

            if ($spending = Spending::where('transaction_id', $id)->count()) {
                Spending::where('transaction_id', $id)->delete();
            }

            $request->session()->flash('key', 'successfuly deleted transaction');
            return redirect()->back();
        } else {
            $request->session()->flash('key', 'error deleted transaction');
            return redirect()->back();
        }
    }
}
