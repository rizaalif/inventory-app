<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'user_id', 'name', 'size', 'path', 'state'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
