<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name', 'address', 'phone',
    ];

    public function spendings()
    {
        return $this->hasMany('App\Model\Spending');
    }
}
