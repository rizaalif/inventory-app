<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'name', 'address', 'phone'
    ];

    public function products()
    {
        return $this->hasMany('App\Model\Product');
    }

    public function incomings()
    {
        return $this->hasMany('App\Model\Incoming');
    }
}
