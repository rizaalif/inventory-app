<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Incoming extends Model
{
    protected $fillable = [
        'transaction_id', 'supplier_id', 'product_id', 'total_products',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Model\Transaction');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Model\Supplier');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
