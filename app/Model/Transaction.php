<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'type', 'information', 'date'
    ];

    public function incomings()
    {
        return $this->hasMany('App\Model\Incoming');
    }
}
