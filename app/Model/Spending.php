<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Spending extends Model
{
    protected $fillable = [
        'transaction_id', 'client_id', 'product_id', 'total_products',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Model\Transaction');
    }

    public function client()
    {
        return $this->belongsTo('App\Model\Client');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
