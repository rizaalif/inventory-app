<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'stock', 'supplier_id'
    ];

    public function supplier()
    {
        return $this->belongsTo('App\Model\Supplier');
    }

    public function incomings()
    {
        return $this->hasMany('App\Model\Incoming');
    }
}
